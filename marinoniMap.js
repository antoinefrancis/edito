var map = L.map('map').setView([45.51887,-73.56466], 18);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> afrancis'
}).addTo(map);

L.marker([45.51887,-73.56466]).addTo(map)
    .openPopup();